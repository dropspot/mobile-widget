#
# Copyright 2014 Dropspot GmbH
# Licensed under MIT
# (https://bitbucket.org/dropspot/mobile-widget/src/master/LICENSE)
#

#!/bin/sh

if [ -z $1 ]
then
    echo "Pass an argument defining one of the following environments: 'dev', 'staging', 'production'";
else
    npm install
    bower install
    grunt $1
fi
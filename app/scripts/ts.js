/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/mobile-widget/src/master/LICENSE)
 */

function Collection(apiPath, slug, link) {
    this.apiPath = apiPath;
    this.mapID = '#map';
    this.loadingDIV = document.getElementById('loading');
    this.slug = slug;
    this.link = link;
    this.publisher = 'tagesspiegel';
    this.spots = [];
    this.spotIndex = 0;
    this.map = this.centeredSpot = null;
    this.wasFaderShown = true; //disable the fader
    this.isRightBtnHintHidden = false;
    this.activeMarkerIcon = {
        url: "/images/sprite.png",
        size: new google.maps.Size(25, 26),
        origin: new google.maps.Point(0, 185),
        anchor: new google.maps.Point(12, 12)
    };
    this.markerIcon = {
        url: "/images/sprite.png",
        size: new google.maps.Size(25, 26),
        origin: new google.maps.Point(0, 220),
        anchor: new google.maps.Point(12, 12)
    };

    this.getSpots();

    /**
     * Private method for showing/hiding Prev/Next buttons
     */
    this.checkPrevNext = function () {
        // if current spot is not the first one, make [prev] button visible
        var btnPrev = document.getElementById('btn_prev');
        var btnNext = document.getElementById('btn_next');

        if(this.spotIndex > 0 && (btnPrev.style.display === 'none' || btnPrev.style.display === '')) {
            btnPrev.style.display = 'block';
        }

        if(this.spotIndex === 0 && (btnPrev.style.display !== 'none' || btnPrev.style.display !== '')) {
            btnPrev.style.display = 'none';
        }

        if(this.spotIndex === this.spots.length-1 && (btnNext.style.display !== 'none' || btnNext.style.display !== '')) {
            btnNext.style.display = 'none';
        }

        if(this.spotIndex < this.spots.length-1 && (btnNext.style.display === 'none' || btnNext.style.display === '')) {
            btnNext.style.display = 'block';
        }
    };

    /**
     * Refresh map markers
     */
    this.refreshMarkers = function() {
        var oldMarkers = this.map.markers;
        this.map.removeMarkers();
        this.map.addMarkers(oldMarkers);
    };

    /**
     * Fade out the right button hint after [>] is tapped for the first time
     */
    this.rightHint = function() {
        if(!this.isRightBtnHintHidden) {
            Lib.visual.fade('out', document.getElementById('btn-right-hint'), 500, true);
            this.isRightBtnHintHidden = !this.isRightBtnHintHidden;
            document.getElementById("btn_next").className = "bm-controller sprite btn_next right"
        }
    }
}

Collection.prototype = {
    setCenter: function (spot) {
        this.map.setCenter(spot.latitude, spot.longitude, function () {
        });
    },

    showSpot: function (spot) {
        this.setCenter(spot);
        if (!spot.isForeign || !spot.content_links.links[0]) {
            // if the spot does not belong to the article or has no links
            // display the spot's name as a text
            document.getElementById("spot_name").innerHTML = spot.name;
        } else {
            // otherwise display the spot's name as a link
            document.getElementById("spot_name").innerHTML =
                '<a id="article-link" target="_parent" href="' +
                spot.content_links.links[0] +
                '">' +
                spot.name + ' &raquo;</a>';
            document.getElementById("article-link").onclick = function () {
                collection.openArticleLink();
                return false;
            };
        }
    },

    recalculateCenter: function () {
        // check if map is loaded
        if(this.map) {
            this.map.refresh();
            if (this.centeredSpot)
                this.setCenter(this.centeredSpot);
        }
    },

    addMarkerToSpot: function (spot, current, index) {
        var that = this;
        var marker = {
            lat: spot.latitude,
            lng: spot.longitude,
            title: spot.name,
            index: index,
            icon: this.markerIcon,
            click: function(marker) {
                if(that.spots[marker.index].current === false) {
                    ga('send', 'event', 'Map', 'Spot Click', that.spots[marker.index].spot.slug);
                    marker.icon = that.activeMarkerIcon;
                    that.spots[that.spotIndex].current = false;
                    that.spots[that.spotIndex].marker.icon = that.markerIcon;
                    that.spots[marker.index].current = true;
                    that.map.markers[that.spotIndex].icon = that.markerIcon;
                    that.refreshMarkers();
                    that.spotIndex = marker.index;
                    that.showSpot(that.spots[marker.index].spot);
                    that.checkPrevNext();
                    that.rightHint();
                }
            }
        };
        if (current)
            marker.icon = that.activeMarkerIcon;
        return marker
    },

    getSpots: function () {
        var that = this;
        var addSpots = function (spots) {
            if (spots.length > 0) {
                document.getElementById('btn_next').style.display = 'block';
                document.getElementById('btn-right-hint').style.display = 'block';
            }
            for (var i = 0; i < spots.length; i++) {
                var spot = spots[i];
                var current = false;
                var marker = that.addMarkerToSpot(spot, current, i);

                // initialize the map with first spot coordinates
                if (that.spots.length === 0 && spot) {
                    current = true;
                    that.map = new GMaps({
                        div: that.mapID,
                        lat: spot.latitude,
                        lng: spot.longitude,
                        zoom: 15,
                        disableDefaultUI: true,
                        mapTypeControl: false,
                        draggable: true,
                        scrollwheel: false,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.LEFT_CENTER
                        },
                        disableDoubleClickZoom: false,
                        dragstart: function() {
                            ga('send', 'event', 'Map', 'Drag Map', that.spots[that.spotIndex].spot.slug);
                        },
                        zoom_changed: function() {
                            ga('send', 'event', 'Map', 'Zoom Map', that.spots[that.spotIndex].spot.slug);                        }
                    });
                    marker = that.addMarkerToSpot(spot, current, 0);
                    that.centeredSpot = spot;
                    document.getElementById('bm-header').removeChild(that.loadingDIV);
                    that.showSpot(that.centeredSpot);
                }
                spot = {spot: spot, marker: marker, current: current};
                that.spots.push(spot);
                that.map.addMarker(spot.marker);
            }
        };
        // get the spots with a link similar to the link of the article
        var url;
        if(that.slug && that.slug !== '') {
            url = that.apiPath + '/collections/' + that.slug + '/spots/.json?page_size=30&link=' + that.link;
        } else
        {
            url = that.apiPath + '/users/DerTagesspiegel/spots/.json?page_size=30&link=' + that.link;
        }
        Lib.ajax.getJSON({
            url: url,
            type: 'json'
        }, function (data) {
            var articleSpots = JSON.parse(data).results;
            if (articleSpots.length === 0) {
                // if there is no spots matching the article's link, disable the fader
                that.wasFaderShown = true;
                var url;
                if(that.slug && that.slug !== '') {
                    url = that.apiPath + '/collections/' + that.slug + '/spots/.json?page_size=30';
                } else
                {
                    url = that.apiPath + '/users/DerTagesspiegel/spots/.json?page_size=30';
                }
                Lib.ajax.getJSON({
                    url: url,
                    type: 'json'
                }, function (all_data) {
                    articleSpots = JSON.parse(all_data).results;
                    // marking the spots as foreign
                    for(var i=0; i< articleSpots.length; i++) {
                        articleSpots[i].isForeign = true;
                    }
                    addSpots(articleSpots);
                })
            } else {
                // user the first collection slug of the spot as a collection for the further spots
                that.slug = articleSpots[0].collection_slugs.split(',')[0];

                // get another 10 spots and remove already retrieved spots (spots that correspond to the article)
                var foreignSpotsUrl;
                if(articleSpots.length && articleSpots.length > 0) {
                    // if one or more spots corresponding to the article were returned,
                    // prepare the url for requesting more spots from the collection sorted by the distance
                    // to the last article spot
                    var lastArticleSpot = articleSpots[articleSpots.length - 1];
                    foreignSpotsUrl = that.apiPath + '/collections/' + that.slug + '/spots/.json?page_size=30&center=' +
                        lastArticleSpot.latitude + ',' + lastArticleSpot.longitude;
                } else {
                    // if there is no spots corresponding to the article, just get 30 spots
                    if (that.slug) {
                        //from the same collection
                        foreignSpotsUrl = that.apiPath + '/collections/' + that.slug + '/spots/.json?page_size=30';
                    } else {
                        //of just from the Tagesspiege user if the collection is not known ('same code for all collections' widget)
                        foreignSpotsUrl = that.apiPath + '/users/DerTagesspiegel/spots/.json?page_size=30';
                    }
                }
                Lib.ajax.getJSON({
                    url: foreignSpotsUrl,
                    type: 'json'
                }, function (data) {
                    var foreignSpots = JSON.parse(data).results;
                    for (var i = 0; i < foreignSpots.length; i++) {
                        foreignSpots[i].isForeign = true;
                        for (var j = 0; j < articleSpots.length; j++) {
                            if (foreignSpots[i].slug == articleSpots[j].slug) {
                                foreignSpots.splice(i, 1);
                                i--;
                                break;
                            }
                        }
                    }
                    if(foreignSpots[0] && foreignSpots[0].center_distance) {
                        if(foreignSpots[0].center_distance.distance_meter > 100000)
                        {
                            // if next closest foreign spot is further than 100km,
                            // change the text of the 'right button hint'
                            document.getElementById("btn-right-hint-text").innerHTML="Weitere News";
                        }
                    }
                    // concatenate both spot arrays
                    articleSpots = articleSpots.concat(foreignSpots);
                    articleSpots.count += foreignSpots.count;
                    addSpots(articleSpots);
                });
            }
        })
    },

    nextSpot: function () {
        ga('send', 'event', 'Map', 'Next Click', this.spots[this.spotIndex].spot.slug);

        // reset current marker
        this.spots[this.spotIndex].marker.icon = this.markerIcon;
        this.spots[this.spotIndex].current = false;
        if (this.spotIndex < this.spots.length - 1) {
            this.spotIndex++;
            if (this.spots[this.spotIndex].spot.isForeign && !this.wasFaderShown) {
                var fader = document.getElementById('fader');
                this.wasFaderShown = true;
                Lib.visual.fade('in', fader, 500, true);
                setTimeout(function () {
                    Lib.visual.fade('out', fader, 500, true)
                }, 2000);
            }
        } else {
            this.spotIndex = 0;
        }

        this.rightHint();

        // make new current marker
        this.showSpot(this.spots[this.spotIndex].spot);
        this.centeredSpot = this.spots[this.spotIndex].spot;
        this.spots[this.spotIndex].marker.icon = this.activeMarkerIcon;
        this.spots[this.spotIndex].current = true;

        // rerender map marker
        this.map.removeMarkers();
        for (var i = 0; i < this.spots.length; i++) {
            this.map.addMarker(this.spots[i].marker);
        }

        this.checkPrevNext();
    },
    prevSpot: function () {
        ga('send', 'event', 'Map', 'Previous Click', this.spots[this.spotIndex].spot.slug);

        this.spots[this.spotIndex].marker.icon = this.markerIcon;
        this.spots[this.spotIndex].current = false;
        if (this.spotIndex > 0) {
            this.spotIndex--;
        } else {
            this.spotIndex = this.spots.length - 1;
        }

        // make new current marker
        this.showSpot(this.spots[this.spotIndex].spot);
        this.centeredSpot = this.spots[this.spotIndex].spot;
        this.spots[this.spotIndex].marker.icon = this.activeMarkerIcon;
        this.spots[this.spotIndex].current = true;

        // rerender map marker
        this.map.removeMarkers();
        for (var i = 0; i < this.spots.length; i++) {
            this.map.addMarker(this.spots[i].marker);
        }

        this.checkPrevNext();
    },

    openArticleLink: function () {
        var spotLink = this.spots[this.spotIndex].spot.content_links.links[0];
        ga('send', 'event', 'Map', 'Click Article Link', spotLink);
        ga(function () {
            // making sure that the event is recorded before the redirect occurs
            // see http://stackoverflow.com/questions/8692503/javascript-redirect-with-google-analytics
            setTimeout(function() {
                window.parent.location.href = spotLink;
            }, 250);
        });
    }
};
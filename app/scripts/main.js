/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/mobile-widget/src/master/LICENSE)
 */

/**
 * Implementation of jQuery onready in pure JS
 */
(function(funcName, baseObj) {
    // The public function name defaults to window.docReady
    // but you can pass in your own object and own function name and those will be used
    // if you want to put them in a different namespace
    funcName = funcName || "docReady";
    baseObj = baseObj || window;
    var readyList = [];
    var readyFired = false;
    var readyEventHandlersInstalled = false;

    // call this when the document is ready
    // this function protects itself against being called more than once
    function ready() {
        if (!readyFired) {
            // this must be set to true before we start calling callbacks
            readyFired = true;
            for (var i = 0; i < readyList.length; i++) {
                // if a callback here happens to add new ready handlers,
                // the docReady() function will see that it already fired
                // and will schedule the callback to run right after
                // this event loop finishes so all handlers will still execute
                // in order and no new ones will be added to the readyList
                // while we are processing the list
                readyList[i].fn.call(window, readyList[i].ctx);
            }
            // allow any closures held by these functions to free
            readyList = [];
        }
    }

    function readyStateChange() {
        if ( document.readyState === "complete" ) {
            ready();
        }
    }

    // This is the one public interface
    // docReady(fn, context);
    // the context argument is optional - if present, it will be passed
    // as an argument to the callback
    baseObj[funcName] = function(callback, context) {
        // if ready has already fired, then just schedule the callback
        // to fire asynchronously, but right away
        if (readyFired) {
            setTimeout(function() {callback(context);}, 1);
            return;
        } else {
            // add the function and context to the list
            readyList.push({fn: callback, ctx: context});
        }
        // if document already ready to go, schedule the ready function to run
        if (document.readyState === "complete") {
            setTimeout(ready, 1);
        } else if (!readyEventHandlersInstalled) {
            // otherwise if we don't have event handlers installed, install them
            if (document.addEventListener) {
                // first choice is DOMContentLoaded event
                document.addEventListener("DOMContentLoaded", ready, false);
                // backup is window load event
                window.addEventListener("load", ready, false);
            } else {
                // must be IE
                document.attachEvent("onreadystatechange", readyStateChange);
                window.attachEvent("onload", ready);
            }
            readyEventHandlersInstalled = true;
        }
    }
})("docReady", window);

/**
 * Implementation of jQuery getJSON on pure JS
 */
(function() {
    window.Lib = {
        ajax: {
            xhr: function() {
                return new XMLHttpRequest();
            },
            getJSON: function(options, callback) {
                var xhttp = this.xhr();
                options.url = options.url || location.href;
                options.data = options.data || null;
                callback = callback ||
                    function() {};
                options.type = options.type || 'json';
                var url = options.url;

                if (options.type == 'jsonp') { // JSONP
                    window.jsonCallback = callback; // Now our callback method is globally visible
                    var $url = url.replace('callback=?', 'callback=jsonCallback');
                    var script = document.createElement('script');
                    script.src = $url;
                    document.body.appendChild(script);
                }

                xhttp.open('GET', options.url, true);
                xhttp.send(options.data);
                xhttp.onreadystatechange = function() {
                    if (xhttp.status == 200 && xhttp.readyState == 4) {
                        callback(xhttp.responseText);
                    }
                };
            }

        },
        visual: {
            /**
             * jQuery style fade in/out
             *
             * @param type 'in' or 'out'
             * @param el
             * @param duration
             * @param IEsupport
             */
            fade:  function (type, el, duration, IEsupport) {
                var isIn = (type == 'in'),
                    IE = (IEsupport) ? IEsupport : false,
                    opacity = isIn ? 0 : 1,
                    interval = 50,
                    gap = interval / duration;

                if (isIn) {
                    el.style.display = 'block';
                    el.style.opacity = opacity;
                    if (IE) {
                        el.style.filter = 'alpha(opacity=' + opacity + ')';
                        el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity + ')';
                    }
                }

                function func() {
                    opacity = isIn ? opacity + gap : opacity - gap;
                    el.style.opacity = opacity;
                    if (IE) {
                        el.style.filter = 'alpha(opacity=' + opacity * 100 + ')';
                        el.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(Opacity=' + opacity * 100 + ')';
                    }
                    if (opacity <= 0 || opacity >= 1) {
                        window.clearInterval(fading);
                    }
                    if (opacity <= 0) {
                        el.style.display = "none";
                    }
                }

                var fading = window.setInterval(func, interval);
            }
        }
    };

    function getQueryVariable(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i=0;i<vars.length;i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
    }

    var collection_slug = getQueryVariable("collection");
    var pageLink = getQueryVariable("article");

    var parser = document.createElement('a');
    parser.href = pageLink;
    pageLink = parser.host + parser.pathname;

    this.collection = new Collection(API_PATH, collection_slug, pageLink);
}());

docReady(function() {
    document.getElementById("btn_prev").onclick = function () {collection.prevSpot()};
    document.getElementById("btn_next").onclick = function () {collection.nextSpot()};
    document.getElementById("bm-header").onclick = function () {collection.recalculateCenter()};

    window.onresize = function () {
        this.collection.recalculateCenter();
    };

    var iOS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? true : false );
    var ios_ad = document.getElementById('bm-footer');
    var map_wrapper = document.getElementById('map-wrapper');
    if(!iOS){
      ios_ad.style.display = 'none';
      map_wrapper.style.top = '0';
    }

});

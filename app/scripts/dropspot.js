/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT
 * (https://bitbucket.org/dropspot/mobile-widget/src/master/LICENSE)
 */

var WIDGET_PATH = '@@WIDGET_PATH';

var STYLE = {
   'bild-mobile': {
        'name': 'bild-mobile',
        'auth': false,
        'map_interact': false,
        'link_filter': true,
        'height': '340px',
        'entry': ''
    },
    'ts-mobile': {
        'name': 'ts-mobile',
        'auth': false,
        'map_interact': false,
        'link_filter': true,
        'height': '340px',
        'entry': '/ts.html'
    }
};

// using dropspot.widget as a namespace
var dropspot = dropspot || {};
dropspot.widget = dropspot.widget || {};

/**
 * Using anonymous self-executing function to achieve code isolation
 */
(function() {
    var widgetStyle = STYLE['bild-mobile'];

    loadWidgetIFrame();

    /**
     * Create the iFrame with the widget
     */
    function loadWidgetIFrame() {
        var widgetDivs = document.getElementsByClassName('dropspot-collection'),
            slug = '',
            widgetPath = '',
            iFrame,
            widgetDiv;

        for (var i = 0; i < widgetDivs.length; ++i) {
            widgetDiv = widgetDivs[i];
            if (widgetDiv.getAttribute("data-style") && STYLE[widgetDiv.getAttribute("data-style")] !== undefined) {
                widgetStyle = STYLE[widgetDiv.getAttribute("data-style")];
            } else {
                // default style
                widgetStyle = STYLE['ts-mobile'];
            }
            slug = widgetDiv.getAttribute('data-slug');
            if(slug){
                slug = 'collection=' + slug + '&';
            } else {
                slug = '';
            }
            widgetPath = WIDGET_PATH + widgetStyle.entry + '?' + slug + 'article=' + document.URL ;
            if (!widgetDiv.hasChildNodes()) {
                iFrame = document.createElement('iframe');
                iFrame.frameBorder = 0;
                iFrame.width = "100%";
                iFrame.height = widgetStyle.height;
                iFrame.scrolling = "no";
                iFrame.setAttribute("src", widgetPath);
                widgetDiv.appendChild(iFrame);
            }
        }
    }
})();
# README #

### Description ###

A mobile widget for the Dropspot geo-coding platform that publishers can embed into their content to display maps with 
locations that are provided via [Dropspot API](https://https://bitbucket.org/dropspot/dropspot-project/)

### Features at glance ###

* Provides a lightweight widget that can be embedded into web pages of content provides and displays an interactive map
 with spots that correspond to the page the widget is embedded into. The widget requires 
 [Dropspot API](https://bitbucket.org/dropspot/dropspot-project/). Additionally, Dropspot 
 Dashboard can be used for processing the geo-information in the Dropspot backend. 

### System requirements ###

* Web server that can serve static pages
* Access to Dropspot API

### Set up ###

The project is built using [Yeoman](http://yeoman.io). For deploying the application you will need Node.js >0.10.25, 
npm (Node Packet Manager) >1.4.9 and Bower >1.3.5

Follow following steps to deploy the application

* in `config/environments/production.json` set `API_PATH` to point to Dropspot API 
* in `config/environments/production.json` set `WIDGET_PATH` to point at widget's `index.html`
* run `npm install` from the root of the project to install Node.js dependencies
* run `bower install` from the root of the project to install Bower dependencies

On your development environment you can start the application by using Grunt server. Point to the Dropspot API in
`config/environments/production.json` and run `grunt serve`. The application will start on port `8000`.


### Embedding widget ###

```html
<div class="dropspot-collection" data-style="<style>" data-publisher="<publisher>"></div>
<script src="//<WIDGET_PATH>/dropspot.js" type="text/javascript"></script>
```

where `<style>` can be either `bild-mobile` or `ts-mobile` and `<WIDGET_PATH>` must have the same value as `WIDGET_PATH`
in `config/environments/production.json`.

### Contribution guidelines ###

Feel free to create a new Pull request if you want to propose a new feature. 

### Who do I talk to? ###

* If you need development support contact us at admin@drpspt.com

### Other Dropspot Products ###

* [Dropspot API](https://https://bitbucket.org/dropspot/api)
* [Dropspot Dashboard](https://https://bitbucket.org/dropspot/dashboard)
* [Dropspot iOS App](https://https://bitbucket.org/dropspot/iosapp)
* [Dropspot Widget](https://https://bitbucket.org/dropspot/widget)

### License ###

MIT License. Copyright 2014 Dropspot GmbH. [https://bitbucket.org/dropspot/mobile-widget](https://bitbucket.org/dropspot/mobile-widget)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
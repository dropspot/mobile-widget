/*
 * Copyright 2014 Dropspot GmbH
 * Licensed under MIT (https://bitbucket.org/dropspot/widget-bild-mobile/src/master/LICENSE)
 */

/* exported API_PATH */

var API_PATH = '@@API_PATH';